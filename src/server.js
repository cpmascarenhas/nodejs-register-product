const express = require("express")
const app = express();
const cors = require('cors');
const mongoose = require('mongoose')
require('dotenv').config();
const requireDir = require('require-dir');

// const uri = "process.env.DATABASE_CONNECTION_STRING";

app.use(express.json())
app.use(cors());

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

app.listen(process.env.PORT || 3001, () => {
    console.log("rodando na porta 3001")
    mongoose.connect(process.env.DATABASE_CONNECTION_STRING, {
        useUnifiedTopology: true,
        useFindAndModify: true,
        useNewUrlParser: true,
        useCreateIndex: true
    }, (err, response) => {
        if (err) {
            throw new Error('CONNECTION FAILED')
        }
        console.log('Conectado') //retorna um 'conectado' no console caso a conexão dê certo
    })
});


requireDir("./models") 


app.use('/api', require("./routes"));


