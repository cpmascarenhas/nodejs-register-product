const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const ProductSchema = new mongoose.Schema({
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },

    title:{
        type: String,
        require: true,
    },
    description:{
        type: String,
        require: true,
    },
    url:{
        type:String,
        require: true,
    },
    createdAt:{
        type: Date,
        dafault: Date.now,
    },
});

ProductSchema.plugin(mongoosePaginate);

mongoose.model('Product', ProductSchema)